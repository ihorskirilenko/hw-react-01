import React, {Component} from "react";
import styled from "styled-components";


export default class Modal extends Component {

    constructor(props) {
        super(props);
    }

    generateModal = () => {

        const Shadow = styled.div`
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: rgba(0, 0, 0, 0.3);
          display: flex;
        `;

        const Wrapper = styled.div`
          font-family: sans-serif;
          color: white;
          margin: 0 auto;
          align-self: center;
          user-select: none;
          display: flex;
          flex-direction: column;
          width: 40vw;
          box-sizing: border-box;
          background-color: #e74c3c;
        `;

        const Header = styled.div`
          display: flex;
          background-color: #d44637;
          font-size: 2rem;
          padding: 1rem;
        `

        const Title = styled.div`
          flex-grow: 1;
        `;

        const CloseBtn = styled.div``;

        const Text = styled.div`
          font-size: 1rem;
          line-height: 2rem;
          padding: 0.5rem;
          justify-self: center;
          align-self: center;
          text-align: center;
        `;

        const Buttons = styled.div``;


        return (
            <Shadow onClick={this.props.onModalClose}>
                <Wrapper onClick={(ev) => ev.stopPropagation()}>
                    <Header>
                        <Title>{this.props.header}</Title>
                        {this.props.isCloseButton ? <CloseBtn onClick={this.props.onModalClose}>X</CloseBtn> : false}
                    </Header>
                    <Text>{this.props.text}</Text>
                    <Buttons>
                        {this.props.actions}
                    </Buttons>
                </Wrapper>
            </Shadow>
        )

    }

    render() { return this.generateModal() }

}
