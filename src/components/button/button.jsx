import React, {Component} from "react";
import styled from 'styled-components'




export default class Button extends Component {

    constructor(props) {
        super(props);
    }

    generateBtn = () => {

        const Btn = styled.button`
          background-color: ${this.props.backgroundColor};
          color: white;
          font-size: 1em;
          margin: 1em;
          padding: 0.25em 1em;
          border: none;
          border-radius: 3px;
        `;

        return <Btn type='button' onClick={this.props.onClick}>{this.props.text}</Btn>

    }

    render() { return this.generateBtn() }

}

