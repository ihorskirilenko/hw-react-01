import logo from './logo.svg';
import './App.css';
import Button from "./components/button/button";
import Modal from "./components/modal/modal";
import React, {Component, useState} from "react";

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal1: false,
            showModal2: false
        }
    }

    render() {
        return (
            <>

                <Button backgroundColor='palevioletred' text='Open first modal' onClick={() => this.setState({showModal1: true})}/>
                <Button backgroundColor='cornflowerblue' text='Open second modal' onClick={() => this.setState({showModal2: true})}/>

                {this.state.showModal1 && <Modal onModalClose={() => this.setState({showModal1: false})}
                    header='Do you want to delete this file?'
                    isCloseButton={true} text='Once you delete this file, it wont be possible to undo this action. Are you sure you want to delete it?'
                    actions={
                        [<Button backgroundColor='red' text='Ok' onClick={() => this.setState({showModal1: false})}/>]
                    }
                    />
                    }

                {this.state.showModal2 && <Modal onModalClose={() => this.setState({showModal2: false})}
                    header='Do you want to save changes?'
                    isCloseButton={false} text='Confirm your changes'
                    actions={
                        [<Button backgroundColor='red' text='Close' onClick={() => this.setState({showModal2: false})}/>,
                        <Button backgroundColor='red' text='Ok' onClick={() => this.setState({showModal2: false})}/>]
                    }
                    />

                    }

            </>
        );
    }

}





